use log::info;
use zbus::Connection;
use color_eyre::eyre::Result;
extern crate pretty_env_logger;

#[tokio::main]
pub async fn main() -> Result<()> {
    pretty_env_logger::init();

    let conn: Connection = Connection::session().await?;
    info!("Connection created: {:?}", conn);

    Ok(())
}

